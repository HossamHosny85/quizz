<?php

namespace App\Http\Controllers;

use App\Models\Guest;
use Illuminate\Http\Request;

class GuestController extends Controller
{

    public function showAll()
    {
        return view('guests', [
            'guests' => Guest::all()
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|unique:guests|email',
        ]);
        $guest = new Guest;
        $guest->first_name = $request->first_name;
        $guest->last_name = $request->last_name;
        $guest->email = $request->email;
        $guest->save();
        $request->session()->put('question_id', 0);
        $request->session()->put('guest_id', $guest->id);
        return redirect()->route('quizz');
    }

}
