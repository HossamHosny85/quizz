<?php

namespace App\Http\Controllers;

use App\Models\Questions;
use Illuminate\Http\Request;

class QuestionsController extends Controller
{

    public function showAll()
    {
        return view('questions', [
            'questions' => Questions::all()
        ]);
    }

    public function add() 
    {
        return view('add-question');
    }

    public function edit(Request $request) 
    {
        $question = Questions::find($request->id);
        return view('edit-question', [
            'question' => $question
        ]);
    }

    public function delete(Request $request)
    {
        $question = Questions::find($request->id);
        $question->delete();
        return redirect()->route('questions');
    }

    public function store(Request $request)
    {
        $request->validate([
            'question' => 'required|string',
        ]);
        $question = new Questions;
        $question->question = $request->question;
        $question->save();
        return redirect()->route('questions');
    }

    public function update(Request $request)
    {
        $request->validate([
            'question' => 'required|string',
        ]);
        $question = Questions::find($request->id);
        $question->question = $request->question;
        $question->save();
        return redirect()->route('questions');
    }

}
