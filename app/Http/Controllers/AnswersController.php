<?php

namespace App\Http\Controllers;

use App\Models\Answers;
use App\Models\Questions;
use Illuminate\Http\Request;

class AnswersController extends Controller
{
    public function showAll()
    {
        return view('all-answers', [
            'answers' => Answers::all()
        ]);
    }

    public function showAllByQuestionId(Request $request)
    {
        $question_id = $request->id;
        $question = Questions::find($question_id);
        $answers = Answers::where('question_id', $question_id)->get();
        return view('question-answers', [
            'question' => $question,
            'answers' => $answers
        ]);
    }

    public function showNext(Request $request)
    {
        $question_id = $request->session()->get('question_id');
        if($question_id == 0) {
            $question = Questions::first();
        } else {
            $question = Questions::where('id','>',$question_id)->first();
        }

        if(!$question && ($question_id > 0)) {
            return redirect()->route('congrats');
        } else {
            $request->session()->put('question_id', $question->id);
            return view('quizz', [
                'question' => $question
            ]);
        }
    }

    public function store(Request $request)
    {
        $guest_id = $request->session()->get('guest_id');
        $question_id = $request->session()->get('question_id');
        $request->validate([
            'answer' => 'required|string',
        ]);
        $answer = new Answers;
        $answer->guest_id = $guest_id;
        $answer->question_id = $question_id;
        $answer->answer = $request->answer;
        $answer->save();
        return redirect()->route('quizz');
    }
}
