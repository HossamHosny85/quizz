<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Answers extends Model
{
    use HasFactory;
    protected $fillable = [
        'guest_id',
        'question_id',
        'answer',
    ];

    public function guest()
    {
        return $this->belongsTo(Guest::class);
    }

    public function question()
    {
        return $this->belongsTo(Questions::class);
    }
}
