<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="bg-dark">
        <div class="d-flex align-items-center justify-content-center h-100">
            <div class="container w-75">
                <div class="row justify-content-center">
                    <div class="col-auto pb-2">
                        <img src="{{ asset('images/quizz-logo.png') }}" height="200" />
                    </div>
                </div>

                <div class="row justify-content-center">
                    <div class="col-8 bg-secondary overflow-hidden shadow rounded-3">
                        <div class="p-4">
                            <h2 class="text-white text-center">Start Your Quizz</h2>

                            <div>
                                <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                                    <form method="POST" action="{{ route('save-guest') }}">
                                        @csrf
                                        <div class="row align-items-center justify-content-center">
                                            <div class="col-8 mb-3">
                                                <input type="text" class="form-control" name="first_name" placeholder="First Name" aria-label="First Name" required autofocus />
                                            </div>                                        
                                        </div>
                                        <div class="row align-items-center justify-content-center">
                                            <div class="col-8 mb-3">
                                                <input type="text" class="form-control" name="last_name" placeholder="Last Name" aria-label="Last Name" required />
                                            </div>                                        
                                        </div>
                                        <div class="row align-items-center justify-content-center">
                                            <div class="col-8 mb-3">
                                                <input type="email" class="form-control" name="email" placeholder="Email Address" aria-label="Email Address" required />
                                            </div>
                                        </div>
                                        <div class="row align-items-center justify-content-center">
                                            <div class="col-auto">
                                                <button type="submit" class="btn btn-dark">{{ __('Start Quizz') }}</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        @if ($errors->any())
        <div role="alert" class="fixed-top mt-5 col-8 mx-auto align-items-center align-content-center alert alert-danger alert-dismissible fade show">
            <ul class="mb-0">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif
    </body>
</html>
