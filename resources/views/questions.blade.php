<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Questions List') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                @if (count($questions) < 1)
                <div class="p-6 bg-white border-b border-gray-200">
                    You did not add any questions yet!
                </div>
                @else
                    @foreach ($questions as $question)
                    <div class="p-6 bg-white border-b border-gray-200 grid gap-4 grid-cols-2">
                        <div class="items-center justify-start grid grid-cols-1">{{ $question->question }}</div>
                        <div class="flex items-center justify-end gap-4">
                            <a href="{{ route('question-answers', ['id' => $question->id]) }}" class="py-2 px-4 font-semibold uppercase text-xs rounded-lg shadow-md text-white bg-green-500 hover:bg-green-700">Show Answers</a>
                            <a href="{{ route('edit-question', ['id' => $question->id]) }}" class="py-2 px-4 font-semibold uppercase text-xs rounded-lg shadow-md text-white bg-indigo-500 hover:bg-indigo-700">Edit</a>
                            <a href="{{ route('delete-question', ['id' => $question->id]) }}" class="py-2 px-4 font-semibold uppercase text-xs rounded-lg shadow-md text-white bg-red-500 hover:bg-red-700">Delete</a>
                        </div>
                    </div>
                    @endforeach
                @endif
                <div class="p-6 text-right">
                    <a href="{{ route('add-question') }}" class="py-2 px-4 font-semibold uppercase text-xs rounded-lg shadow-md text-white bg-blue-500 hover:bg-blue-700">Add Question</a>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
