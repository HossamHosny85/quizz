<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="bg-dark">
        <div class="d-flex align-items-center justify-content-center h-100">
            <div class="container w-75">
                <div class="row justify-content-center">
                    <div class="col-auto pb-2">
                        <img src="{{ asset('images/quizz-logo.png') }}" height="200" />
                    </div>
                </div>

                <div class="row justify-content-center">
                    <div class="col-8 bg-secondary overflow-hidden shadow rounded-3">
                        <div class="p-4">
                            <h2 class="text-white text-center">User Exists</h2>
                            <p class="text-white text-center">It seems like the email address you are trying to use has already been used here before, please try again with a different email address! <a href="{{ route('welcome') }}">Click Here</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    </body>
</html>
