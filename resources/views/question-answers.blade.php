<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Answers for ') }}"{{ $question->question }}"
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                @if (count($answers) < 1)
                <div class="p-6 bg-white border-b border-gray-200">
                    No one registered answers to this question yet!
                </div>
                @else
                    @foreach ($answers as $answer)
                    <div class="p-6 bg-white border-b border-gray-200 grid gap-4 grid-cols-3">
                        <div class="items-center justify-start grid grid-cols-1">{{ $answer->answer }}</div>
                        <div class="items-center justify-start grid grid-cols-1">{{ $answer->guest->first_name." ".$answer->guest->last_name }}</div>
                        <div class="items-center justify-start grid grid-cols-1">{{ $answer->guest->email }}</div>
                    </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</x-app-layout>
