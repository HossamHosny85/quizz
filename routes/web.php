<?php

use App\Http\Controllers\QuestionsController;
use App\Http\Controllers\GuestController;
use App\Http\Controllers\AnswersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Guest Routes */

Route::get('/', function () { return view('welcome'); })->name('welcome');
Route::get('/congrats', function () { return view('congrats'); })->name('congrats');
// Route::get('/user-exists', function () { return view('user-exists'); })->name('user-exists'); // Unique Email Validation Redirect
Route::get('/quizz', [AnswersController::class, 'showNext'])->name('quizz');

Route::post('/save-guest', [GuestController::class, 'store'])->name('save-guest');
Route::post('/save-answer', [AnswersController::class, 'store'])->name('save-answer');


/* Admin Routes */

Route::get('/dashboard', function () { return view('dashboard'); })->middleware(['auth'])->name('dashboard');
Route::get('/questions', [QuestionsController::class, 'showAll'])->middleware(['auth'])->name('questions');
Route::get('/add-question', [QuestionsController::class, 'add'])->middleware(['auth'])->name('add-question');
Route::get('/edit-question', [QuestionsController::class, 'edit'])->middleware(['auth'])->name('edit-question');
Route::get('/delete-question', [QuestionsController::class, 'delete'])->middleware(['auth'])->name('delete-question');
Route::get('/question-answers', [AnswersController::class, 'showAllByQuestionId'])->middleware(['auth'])->name('question-answers');
Route::get('/answers', [AnswersController::class, 'showAll'])->middleware(['auth'])->name('answers');

Route::post('/save-question', [QuestionsController::class, 'store'])->middleware(['auth'])->name('save-question');
Route::post('/update-question', [QuestionsController::class, 'update'])->middleware(['auth'])->name('update-question');

require __DIR__.'/auth.php';

